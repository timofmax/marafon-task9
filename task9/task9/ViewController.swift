//
//  ViewController.swift
//  task9
//
//  Created by Timofey Privalov on 27.03.2023.
//

import UIKit

class ViewController: UIViewController {
    private lazy var layout: CollectionViewLayout = {
        let layout = CollectionViewLayout.init()
        layout.scrollDirection = .horizontal
        layout.itemSize.width = 200
        layout.itemSize.height = 350
        let insets = UIEdgeInsets(top: 100,left: 0,
                                  bottom: 100, right: 0)
        layout.sectionInset = insets
        return layout
    }()
    
    private lazy var collection: UICollectionView = {
        let view = UICollectionView(frame:.zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.dataSource = self
        view.register(CollectionViewCell.self, forCellWithReuseIdentifier: CollectionViewCell.reuseID)
        view.contentInset = .init(top: 0, left: 10, bottom: 0, right: 10)
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        title = "Collection"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        view.backgroundColor = .white
        view.addSubview(collection)
        NSLayoutConstraint.activate([
            collection.topAnchor.constraint(equalTo: view.topAnchor),
            collection.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collection.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collection.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

extension ViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 57
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: CollectionViewCell.reuseID,
            for: indexPath
        ) as? CollectionViewCell else {
            fatalError("Wrong cell")
        }
        cell.backgroundColor = .lightGray
        return cell
    }
}

class CollectionViewCell: UICollectionViewCell {
    static let reuseID = "CollectionViewCell"
}

class HeaderViewCell: UICollectionReusableView {
    let reuseID = "HeaderViewCell"
    
    private var title: UILabel = {
        let view = UILabel(frame: .zero)
        view.text = "Collection View"
        return view
    }()
    
    func configure(){
        backgroundColor = .systemBrown
        addSubview(title)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        title.frame = bounds
    }
}

class CollectionViewLayout: UICollectionViewFlowLayout {
    private var spacePrevious: CGFloat = 0
    private var nowItem = 0
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let collection = collectionView else {
            return super.targetContentOffset(forProposedContentOffset: proposedContentOffset, withScrollingVelocity: velocity)
        }

        let itemCount = collection.numberOfItems(inSection: 0)

        if spacePrevious > collection.contentOffset.x && velocity.x < 0 {
            nowItem = max(nowItem - 2, 0)
        } else if spacePrevious < collection.contentOffset.x && velocity.x > 0 {
            nowItem = min(nowItem + 2, itemCount - 1)
        }

        let itemW = itemSize.width
        let sp = minimumLineSpacing
        let edge = collection.layoutMargins.left
        let offset = (itemW + sp) * CGFloat(nowItem) - (edge + sp)
        spacePrevious = offset
        return CGPoint(x: offset, y: proposedContentOffset.y)
    }
}
